module.exports = {
  // Basic, simple events
  Events: require("./stuff/events"),

  // An Object containing handy functions
  Functions: require("./stuff/functions.js"),

  // A Class containing every discord function
  Discord: require("./stuff/discord.js"),

  // Trivia questions
  Trivia: require("./stuff/trivia.js"),

  // Canvas Functions
  Canvas: require("./stuff/canvas.js"),

  // SQL Class
  SQL: require("./stuff/canvas.js")
}
