# 1.1.0 (Current)
Added a ton of new features, and a ton of other stuff too
### Features Added
 - SQL Support
 - Discord Support
 - Trivia Support
 - Canvas Support
### Bug Fixes
 - Fixed some crap about the `check` function

## 1.0.1
### Bug Fixes:
 - Made the module actually export stuff this time lol

## 1.0.0
Initial Release